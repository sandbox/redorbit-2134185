<?php 

/**
 * 
 * Translate permissions per Role and Language (translate_role_permissions)
 *
 * With the i18n module, translate permissions can only be set globally. If a user of a given role has 
 * language translate permissions, he will be able to translate into all available languages.
 *
 * The existing module i18n translation_access module allows to set language specific 
 * translation permissions for dedicated users but not for roles. 
 *
 * This little helper module allows to set translate permissions per language and roles.
 *
 * Developed by <info[AT]redorbit[DOT]de>
 * http://www.redorbit.de 
 * 
 */

/**
 * Implements hook_permission().
 */
function translate_role_permissions_permission() {
  foreach (locale_language_list() as $lang_code => $language) {
		$perms[check_plain($lang_code) . ' translations'] = array(
		  'title' => t('Translate to %language', array('%language' => $language)),
		);
	}
  return $perms;
}

/**
 * Implements hook_node_access().
 */
function translate_role_permissions_node_access($node, $op, $account) {
	if(isset($node->language)){
		$type = is_string($node) ? $node : $node->type;
		$lang = check_plain($node->language);		
		if ($op == 'create' && !user_access(check_plain($lang) . ' translations')  ) {
				 return NODE_ACCESS_DENY;
		}
		if ($op == 'update' && !user_access(check_plain($lang) . ' translations')  ) {
				 return NODE_ACCESS_DENY;
		}

		if ($op == 'delete' && !user_access(check_plain($lang) . ' translations')) {
			return NODE_ACCESS_DENY;
		}
	  // Returning nothing from this function would have the same effect.
		return NODE_ACCESS_IGNORE;
	}
}

/**
 * Implements hook_form_alter().
 */
function translate_role_permissions_form_alter(&$form, &$form_state, $form_id) {
	if (strpos($form_id, '_node_form') !== FALSE) {
		if (isset($form_state['node'])) {
			$node = $form_state['node'];
			if (!isset($node->nid) && isset($form['language']) || isset($node->is_new) && isset($form['language'])) {
				$form['#after_build'][] = 'translate_role_permissions_after_build';
			}
			else if(isset($node->nid) && isset($form['language'])){
				$form['language']['#disabled'] = true;
			}
		}
	}
}

/**
 * Implements form_after_build().
 */
function translate_role_permissions_after_build($form, &$form_state) {
	$form['language']['#options'] = check_permissions($form['language']['#options']);
	return $form;
}

/**
 * Check for which languages the user has change permissions
 */
function check_permissions($avaibleLang){  
	foreach($avaibleLang AS $avaible=>$value){
	  if ($avaible != 'und') {
		if (!user_access(check_plain($avaible) . ' translations')  ) {			
			unset($avaibleLang[$avaible]);	 
		}
	  }
	}
	return $avaibleLang;	
}