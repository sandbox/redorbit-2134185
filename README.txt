
TRANSLATE ROLE PERMISSIONS
http://drupal.org/project/translate_role_permission

DESCRIPTION
With the i18n module, translate permissions can only be set globally. If a user of a given role has 
language translate permissions, he will be able to translate into all available languages.

The existing module i18n translation_access module allows to set language specific 
translation permissions for dedicated users but not for roles. 
This little helper module allows to set translate permissions per language and roles.

INSTALL
Install module as usual

SETTINGS 
Path: www.your-domain.com/admin/people/permissions#module-translate_role_permissions

DEPENDENCIES
i18n module

CONTACT <info[AT]redorbit[DOT]de>
http://www.redorbit.de
